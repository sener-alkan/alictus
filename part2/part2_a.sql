-- Check Customers Dataset
SELECT * FROM customers LIMIT 10;

-- Check Orders Dataset
SELECT * FROM orders LIMIT 10;

-- Check LineItem Dataset
SELECT * FROM lineitem LIMIT 10;


-- Join Datasets and Find Average Revenue of Automobile Segment
-- rev = EXTENDEDPRICE * (1 - DISCOUNT)

SELECT AVG(l.EXTENDEDPRICE * (1 - l.DISCOUNT) ) as avg_revenue 
    FROM customers_temp c
    LEFT JOIN orders_temp o
    ON o.CUSTKEY = c.CUSTKEY
    LEFT JOIN listItem_temp l
    ON o.ORDERKEY = l.ORDERKEY
    WHERE MKTSEGMENT = 'AUTOMOBILE'

-- Join Datasets and Find Average Revenue of HouseHold Segment

SELECT AVG(l.EXTENDEDPRICE * (1 - l.DISCOUNT) ) as avg_revenue 
    FROM customers_temp c
    LEFT JOIN orders_temp o
    ON o.CUSTKEY = c.CUSTKEY
    LEFT JOIN listItem_temp l
    ON o.ORDERKEY = l.ORDERKEY
    WHERE MKTSEGMENT = 'HOUSEHOLD'