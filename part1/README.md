## Analytic Solutions

As you stated in the question, the data volume is increasing day by day. When you design the architecture of backend side, you need to think about the scalibility, configurability, consistency and avaibility of the system. Besides that, CAP theorem explains about distributed data storage in big data environments.

To summarize CAP theorem, there are 3 main components as below:

 - Consistency: All records must be consistent.
 - Availability: When I reach one of the servers, it should answer me, not keep me waiting.
 - Partition-tolerance: Tolerate messages being distributed between servers over the network. The message may not go at all, it may go slowly, it may disappear etc.

In distributed systems, at most two of these three concepts can be provided at the same time. Morever, you need to choose the database in CAP triangle (Figure 1). Which features are important for your system ? Which database will be most sufficient ?

![Figure 1](https://www.researchgate.net/profile/Francis-Kagai/publication/332752603/figure/fig1/AS:753307555483649@1556613925550/Triangular-illustration-of-the-CAP-Theorem.ppm)

Document-oriented databases (NoSQL) are popular in order to design backend systems. However, when you have NoSQL databases, you need to transform (ETL) the key-value store to RDB systems or DWH systems in order to establish analytical operation. Otherwise, when you have a prod RDS databases (Postgre, MySQL, Oracle etc.), you can design data-flow as below Figure 2.

![Figure 2 ](https://d2908q01vomqb2.cloudfront.net/fc074d501302eb2b93e2554793fcaf50b3bf7291/2021/08/10/Fig5-RedshiftFed-1024x612.png)