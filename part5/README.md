# QUESTION 5
Write a technical question that, you think, is fairly important and relevant with your envisaged role in the company and/or important showing off your skills, know-how, perspective Etc. And answer it.

# ANSWER
What are vertical scaling and horizontal scaling ? Could you please explain the difference?

## Vertical Scaling

Vertical scaling, also known as scaling up, is the scaling by adding more power (CPU, RAM, DISK, etc.) to an existing machine. There are some powerful database servers.

This kind of powerful database server could store and handle lots of data. For example, stackoverflow.com in 2013 had over 10 million monthly unique visitors, but it only had 1 master database.

However, vertical scaling comes with some serious drawbacks:
* You can add more CPU, RAM, etc. to your database server, but there are hardware limits. If you have a large user base, a single server is not enough.
* Greater risk of single point of failures.
* The overall cost of vertical scaling is high. Powerful servers are much more expensive.

## Horizontal Scaling

Horizontal scaling, also known as sharding, is the practice of adding more servers. Figure 1 compares vertical scaling with horizontal scaling.

![Vertical vs Horizontal Scaling](https://www.section.io/assets/images/blog/featured-images/horizontal-vs-vertical-scaling-diagram.png)


Sharding separates large databases into smaller, more easily managed parts called shards. Each shard shares the same schema, though the actual data on each shard is unique to the shard.

User data is allocated to a database server based on user IDs. Anytime you access data, a hash function is used to find the corresponding shard. If the result
equals to 0, shard 0 is used to store and fetch data. If the result equals to 1, shard 1 is used. The same logic applies to other shards.