# Business KPIs
Games we built in Alictus have a business model based on ad-revenue. We utilize both
interstitial ads and rewarded ads to generate revenue. Based on this information:
- What metrics are important for our business, why?
- What should we focus on while creating new games or features?

# Answer
To be honest, I am not familiar with Ad-revenue business model and game industry. However, When I brainstorm and research, you can find the metrics as below.

- **RFM Analyis (Recency, Frequency and Monetary)**
    - We can group and rank customers based on RFM analysis in order to perform targeted marketing campaigns.
- **Cohort Analysis**
    - We can break the revenue and customers dataset using groups (for instance: daily basis) and we can perform targeted marketing campaigns.
- **K-Factor**
    - K-Factor shows you that how many organic users you get as the result of a paid UA campaign.
    - K = number of invites sent by each user / conversion rate of each invite
- **Daily/Monthly Active Users**
- **Retention Rate**
- **Lifetime Value**
    - **LTV** = Number of Days of Engagement * Average Spend Per Day
- **Churn Analysis (Uninstall)**

## What should we focus on while creating new games or features?

Basically, it depends on the team vision and company strategies. If we want to vary revenue(not only ad-revenue system) item-based games and features can be useful for cash flow.

Otherwise, we can analyze and summarize above metrics to develop new features and new games using marketing campaigns.
