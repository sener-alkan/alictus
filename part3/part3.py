import findspark
findspark.init("/opt/manual/spark")
from pyspark.sql import SparkSession, functions as F
from pyspark.sql.types import *
from os import listdir
from os.path import isfile, join
import datetime

today = datetime.datetime.now()
now = datetime.datetime(today.year, today.month, today.day)

spark = SparkSession.builder \
    .appName("Case Part3") \
    .getOrCreate()

spark.sparkContext.setLogLevel("ERROR")

# List small lineitem dataset in related folder 
path = "/home/train/datasets/dataset_2/"
items = [f for f in listdir(path) if isfile(join(path, f))]

# Defining the schema of the empty RDD DataFrame
columns = StructType([
        StructField("ORDERKEY",IntegerType(),True),
        StructField("PARTKEY",IntegerType(),True),
        StructField("SUPPKEY",IntegerType(),True),
        StructField("LINENUMBER",IntegerType(),True),
        StructField("QUANTITY",IntegerType(),True),
        StructField("EXTENDEDPRICE",DoubleType(),True),
        StructField("DISCOUNT",DoubleType(),True),
        StructField("TAX",DoubleType(),True),
        StructField("RETURNFLAG",StringType(),True),
        StructField("LINESTATUS",StringType(),True),
        StructField("SHIPDATE",StringType(),True),
        StructField("COMMITDATE",StringType(),True),
        StructField("RECEIPTDATE",StringType(),True),
        StructField("SHIPINSTRUCT",StringType(),True),
        StructField("SHIPMODE",StringType(),True),
        StructField("COMMENT",StringType(),True),
        StructField("_c16",StringType(),True)])

# Create empty RDD Dataframe
emp_RDD = spark.sparkContext.emptyRDD()
df = spark.createDataFrame(data=emp_RDD, schema=columns)

for i in items:
    df_item = spark.read.option("header", "false") \
        .option("delimiter", "|") \
        .option("inferSchema", "True") \
        .option("compression","gzip") \
        .csv("file:///home/train/datasets/dataset_2/" + i)
    
    # Append small lineitem datesets to empty RDD Dataframe
    df = df.union(df_item)

# Transforming Date Field Types and Calculating Revenue
df_final = df.withColumn("COMMITDATE", F.to_date(F.col("COMMITDATE"),"yyyy-MM-dd")) \
        .withColumn("SHIPDATE", F.to_date(F.col("SHIPDATE"),"yyyy-MM-dd")) \
        .withColumn("RECEIPTDATE", F.to_date(F.col("RECEIPTDATE"),"yyyy-MM-dd")) \
        .withColumn("REVENUE", F.col("EXTENDEDPRICE") * ( 1 - F.col("DISCOUNT"))) \
        .drop("_c16")

"""
For instance, we have 5 million rows dataset and we want to 
split the dataset to 1 million rows with 5 different files 
"""

df_final_copy = df_final
n_split = 5
# Calculate count of each dataframe rows
len = df.count() // n_split

for i in range(1,6):
    temp_df = df_final_copy.limit(len)
    df_final_copy = df_final_copy.subtract(temp_df)
    
    # Save the output file
    temp_df.write \
        .format("csv") \
        .mode("overwrite") \
        .save("file:///home/train/venvspark/dev/output_data/" + "alictus_output_part_" \
            + str(i) + "_" + now.strftime("%Y_%m_%d"))